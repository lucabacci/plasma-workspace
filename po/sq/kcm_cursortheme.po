# Albanian translation for kdebase-workspace
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdebase-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdebase-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2009-08-08 17:59+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-05-06 02:09+0000\n"
"X-Generator: Launchpad (build 12959)\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr ""

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr ""

#: kcmcursortheme.cpp:302
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr ""

#: kcmcursortheme.cpp:376
#, kde-format
msgid "Unable to create a temporary file."
msgstr ""

#: kcmcursortheme.cpp:387
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr ""

#: kcmcursortheme.cpp:418
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr ""

#: kcmcursortheme.cpp:425
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr ""

#: kcmcursortheme.cpp:434
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr ""

#: kcmcursortheme.cpp:438
#, kde-format
msgid "Overwrite Theme?"
msgstr "Ta Mbishkruaj Temën?"

#: kcmcursortheme.cpp:462
#, kde-format
msgid "Theme installed successfully."
msgstr ""

#: package/contents/ui/Delegate.qml:54
#, kde-format
msgid "Remove Theme"
msgstr "Hiqe Temën"

#: package/contents/ui/Delegate.qml:61
#, fuzzy, kde-format
#| msgid "Remove Theme"
msgid "Restore Cursor Theme"
msgstr "Hiqe Temën"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the mouse cursor theme."
msgstr ""

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Size:"
msgstr ""

#: package/contents/ui/main.qml:134
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "&Install from File…"
msgstr "Instalo Temë të Re..."

#: package/contents/ui/main.qml:140
#, fuzzy, kde-format
#| msgid "Install New Theme..."
msgid "&Get New Cursors…"
msgstr "Instalo Temë të Re..."

#: package/contents/ui/main.qml:157
#, fuzzy, kde-format
#| msgid "Remove Theme"
msgid "Open Theme"
msgstr "Hiqe Temën"

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr ""

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr ""

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr ""

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr ""

#: plasma-apply-cursortheme.cpp:103
#, kde-format
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr ""

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr ""

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr ""

#: xcursor/xcursortheme.cpp:60
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr ""

#~ msgid "Name"
#~ msgstr "Emri"

#~ msgid "Description"
#~ msgstr "Përshkrimi"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "KDE Shqip, Launchpad Contributions: Vilson Gjeci"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "kde-shqip@yahoogroups.com,vilsongjeci@gmail.com"

#~ msgid "Confirmation"
#~ msgstr "Konfirmimi"

#~ msgid "Icons"
#~ msgstr "Ikonat"

#~ msgid "&General"
#~ msgstr "&Të Përgjithshme"

#~ msgid "Advanced"
#~ msgstr "Të Avancuara"

#~ msgid " x"
#~ msgstr " x"

#~ msgid " msec"
#~ msgstr " msec"

#~ msgid "&Acceleration delay:"
#~ msgstr "&Devijimi i përshpejtimit:"

#~ msgid "Acceleration &time:"
#~ msgstr "Koha e &përshpejtimit:"

#~ msgid " pixel/sec"
#~ msgstr " pixel/sec"

#~ msgid "Ma&ximum speed:"
#~ msgstr "Sh&pejtësia maksimale:"

#~ msgid "Acceleration &profile:"
#~ msgstr "Profili i &përshpejtimit:"

#~ msgid "Mouse"
#~ msgstr "Miu"

#~ msgid "(c) 1997 - 2005 Mouse developers"
#~ msgstr "(c) 1997 - 2005 zhvilluesit e miut"

#~ msgid "Patrick Dowler"
#~ msgstr "Patrick Dowler"

#~ msgid "Dirk A. Mueller"
#~ msgstr "Dirk A. Mueller"

#~ msgid "David Faure"
#~ msgstr "David Faure"

#~ msgid "Bernd Gehrmann"
#~ msgstr "Bernd Gehrmann"

#~ msgid "Rik Hemsley"
#~ msgstr "Rik Hemsley"

#~ msgid "Brad Hughes"
#~ msgstr "Brad Hughes"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#~ msgid "Brad Hards"
#~ msgstr "Brad Hards"

#~ msgid " pixel"
#~ msgid_plural " pixels"
#~ msgstr[0] " pixel"
#~ msgstr[1] " pixelë"

#~ msgid " line"
#~ msgid_plural " lines"
#~ msgstr[0] " vijë"
#~ msgstr[1] " vija"

#~ msgid "Mouse type: %1"
#~ msgstr "Lloji i miut: %1"

#~ msgid "Press Connect Button"
#~ msgstr "Shtyp Butonin Lidhu"

#~ msgid "Cordless Mouse"
#~ msgstr "Mi pa Fije"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Mi me Rrotë pa Fije"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "Rrotë Cordless MouseMan"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "Rrotë Cordless TrackMan"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "MX700 Cordless Optical Mouse"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "MX700 Cordless Optical Mouse (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "Mi i panjohur"

#~ msgid "KDE Classic"
#~ msgstr "KDE Klasik"

#~ msgid "No description available"
#~ msgstr "Asnjë përshkrim në dispozicion"
