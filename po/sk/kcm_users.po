# translation of kcm_users.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2020.
# Matej Mrenica <matejm98mthw@gmail.com>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kcm_users\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2022-04-28 08:51+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.04.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Zmeniť heslo"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Heslo"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Potvrďte heslo"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:65
#, kde-format
msgid "Passwords must match"
msgstr "Heslá sa musia zhodovať"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Nastaviť heslo"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Zmeniť heslo peňaženky?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Teraz, keď ste zmenili svoje prihlasovacie heslo, môžete tiež zmeniť heslo "
"svojej predvoleného KWallet, aby sa s ním zhodovalo."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Čo je KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet je správca hesiel, ktorý ukladá vaše heslá pre bezdrôtové siete a "
"ďalšie šifrované zdroje. Je uzamknutý vlastným heslom, ktoré sa líši od "
"vášho prihlasovacieho hesla. Ak sa tieto dve heslá zhodujú, dá sa odomknúť "
"pri prihlásení automaticky, takže nemusíte zadávať heslo KWallet sami."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Zmeniť heslo peňaženky"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Nechať nezmenené"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "Vytvoriť používatelia"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Meno:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Užívateľské meno:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Štandardné"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Administrátor"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Typ účtu:"

#: package/contents/ui/CreateUser.qml:53
#, kde-format
msgid "Password:"
msgstr "Heslo:"

#: package/contents/ui/CreateUser.qml:58
#, kde-format
msgid "Confirm password:"
msgstr "Potvrďte heslo:"

#: package/contents/ui/CreateUser.qml:69
#, kde-format
msgid "Create"
msgstr "Vytvoriť"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Nastaviť odtlačky prstov"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Vyčistiť odtlačky prstov"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Pridať"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "Zrušiť"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Pokračovať"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Hotovo"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Pridáva sa odtlačok"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr "Prosím, opakovane %1 vaše %2 na snímači odtlačkov prstov."

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Odtlačok pridaný"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Vyberte prst na pridanie"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Znovu pridať prst"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Žiadne odtlačky neboli pridané"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "Spravovať používateľov"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Pridať nového používateľa"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Vybrať obrázok"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Zmeniť avatar"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "E-mailová adresa:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Vymazať súbory"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Ponechať súbory"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "Vymazať používateľa..."

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Konfigurovať overenie totožnosti odtlačkom prsta…"

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Odtlačky prstov možno použiť namiesto hesla pri odomykaní obrazovky a "
"poskytovaní oprávnení správcu aplikáciám a programom príkazového riadka, "
"ktoré ich vyžadujú.<nl/><nl/>Prihlásenie do systému pomocou odtlačku prsta "
"zatiaľ nie je podporované."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Zmeniť avatar"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr "Nič to nie je"

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Výbojný plameniak"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Pitaya"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Sladký zemiak"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Okolitý jantár"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Ligotavý slnečný lúč"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Limonáda"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Zelené čaro"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Mäkká Lúka"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "Vlažná sivozelená"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Plasmová modrá"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Pon fialová"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Bajo fialová"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Spálené uhlie"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Dokonalosť papiera"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Kávová hnedá"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Bohaté tvrdé drevo"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Vybrať súbor..."

#: src/fingerprintmodel.cpp:150 src/fingerprintmodel.cpp:257
#, kde-format
msgid "No fingerprint device found."
msgstr "Nenašlo sa žiadne zariadenie na snímanie odtlačkov prstov."

#: src/fingerprintmodel.cpp:316
#, kde-format
msgid "Retry scanning your finger."
msgstr "Skúsiť znova naskenovať prst."

#: src/fingerprintmodel.cpp:318
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Potiahnutie prstom bolo príliš krátke. Skúste to znova."

#: src/fingerprintmodel.cpp:320
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Prst nie je vycentrovaný na čítačke. Skúste to znova."

#: src/fingerprintmodel.cpp:322
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Odstráňte prst z čítačky a skúste to znova."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Registrácia odtlačku prsta zlyhala."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Pre toto zariadenie nie je miesto, odstráňte ostatné odtlačky prstov na "
"pokračovanie."

#: src/fingerprintmodel.cpp:336
#, kde-format
msgid "The device was disconnected."
msgstr "Zariadenie bolo odpojené."

#: src/fingerprintmodel.cpp:341
#, kde-format
msgid "An unknown error has occurred."
msgstr "Vyskytla sa neznáma chyba."

#: src/user.cpp:267
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Nepodarilo sa získať oprávnenia na uloženie používateľa %1"

#: src/user.cpp:272
#, kde-format
msgid "There was an error while saving changes"
msgstr "Nastala chyba pri ukladaní zmien"

#: src/user.cpp:368
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Nepodarilo sa zmeniť veľkosť obrázka: otvorenie dočasného súboru zlyhalo"

#: src/user.cpp:376
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "Nepodarilo sa zmeniť veľkosť obrázka: zápis do dočasného súboru zlyhal"

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "Vaše konto"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Ostatné kontá"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "john.doe@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Roman Paholík"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "wizzardsk@gmail.com"

#~ msgid "Manage user accounts"
#~ msgstr "Spravovať používateľské kontá"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Ján Novák"
